const canvas: any = document.querySelector('canvas');
const ctx = canvas.getContext('2d');

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

var gravity = 3
var balls:Ball[] = []
var mouseBall = null
var ballCounter = 0
var mouseBlock = null
var blocks:Block[] = []
var blockCounter = 0
const interval = 50
var lmx1, lmx2, lmx3
var lmy1, lmy2, lmy3
var timestamp
var speedX
var speedY
var keyPress = []

function keyPressed()
{
    while(keyPress.length >= 1 )
    {
        return true
    }
    return false
}

class Ball 
{

    constructor(
        private x = 0,
        private y = 0,
        private yVel = speedY,
        private xVel = speedX,
        private ballCollided = false
    ){}

    update(){
        if(this.y < canvas.height) 
        {   
            this.y += this.yVel
            this.x += this.xVel
            this.yVel += gravity
        }
        if(this.y>canvas.height-19) 
        {
            this.y = canvas.height-20

            if (this.yVel > 50)
            {
                this.yVel = -this.yVel*0.85
            }

            else if (this.yVel > 30)
            {
                this.yVel = -this.yVel*0.7
            }

            else if (this.yVel > 15)
            {
                this.yVel = -this.yVel*0.5
            }

            else if (this.yVel > 7)
            {
                this.yVel = -this.yVel*0.2
            }

            else
            {
                this.yVel = -this.yVel*0
            }
        }

        if(this.y<19)
        {
            this.y = 20

            this.yVel *= -0.85
            
        }

        if(this.x > canvas.width || this.x < 19)
        {
            if (this.x > canvas.width)
            {
                this.x = canvas.width - 1;
            }
            else
            {
                this.x = 20
            }
            this.xVel *= -0.8   
        }

        for(let i = 0; i<blocks.length; i++)
        {
            if(!this.ballCollided)
            {
                if(this.y>= blocks[i].getY())
                {
                    if(this.x+20 >= blocks[i].getX() && this.x <= blocks[i].getX())
                    {
                        this.xVel *= -1
                        this.x = blocks[i].getX() - 21;
                        //this.ballCollided =false
                    }

                    if(this.x-20 <= blocks[i].getX()+50 && this.x >= blocks[i].getX()+50)
                    {
                        this.xVel *= -1
                        this.x = blocks[i].getX() + 71;
                    }

                    if(this.y+21 >= blocks[i].getY() && this.x <= blocks[i].getX()+50 && this.x >= blocks[i].getX())
                    {
                        this.yVel *= -1
                        this.y = blocks[i].getY() - 21;
                    }
                    else{
                        this.ballCollided = false
                    }
                }

            }
        }
    }

    getY()
    {
        return this.y
    }

    draw() 
    {
        ctx.fillStyle = 'red';
        ctx.beginPath();
        ctx.arc(this.x,this.y, 20, 0, Math.PI*2)
        ctx.closePath()
        ctx.fill()
    }
}

class Block
{
    constructor(
        private x = 0,
        private y = 0,
        private yVel = 0

    ){}

    draw() 
    {
        ctx.fillStyle = 'blue';
        ctx.beginPath();
        ctx.rect(this.x,this.y, 50, 50)
        ctx.closePath()
        ctx.fill()
    }

    update()
    {
        // console.log(this.yVel);
        if(this.y < canvas.height)
        {
            // console.log('reached')
            this.y += this.yVel
            this.yVel += gravity
        }

        if(this.y>canvas.height-49) 
        {
            this.y = canvas.height-50

            if (this.yVel > 50)
            {
                this.yVel = -this.yVel*0.85
            }

            else if (this.yVel > 30)
            {
                this.yVel = -this.yVel*0.7
            }

            else if (this.yVel > 15)
            {
                this.yVel = -this.yVel*0.5
            }

            else if (this.yVel > 7)
            {
                this.yVel = -this.yVel*0.2
            }
        }
    }

    getX()
    {
        return this.x
    }

    getY()
    {
        return this.y
    }
    collideBlock(block) 
    {
        // [(x1, y1), (x2, y2)] top left and bottom right
        let c1 = [(this.x,this.y), (this.x + 50, this.y + 50)]
        let c2 = [(block.x,block.y), (block.x + 50, block.y + 50)]

        return (c1[0][1] > c2[1][1] && c1[1][1] < c2[0][1]) 
            && (c1[0][0] < c2[1][0] && c1[1][0] > c2[0][0]);
    }

    // collidesBall(ball) {

    // }
}

class Player
{
    constructor(
        private color,
        private x = 100,
        private y = 100,
        private xVel = 0,
        private yVel = 0
    ){}
    
    moveUp()
    {
        if(this.yVel>=-15)
        {
        this.yVel -= 1.5
        }
    }

    moveDown()
    {
        if(this.yVel<=15)
        {
        this.yVel += 1.5
        }
        
    }

    moveLeft()
    {
        if(this.xVel>=-15)
        {
        this.xVel -= 1.5
        }
        
    }

    moveRight()
    {
        if(this.xVel<=15)
        {
        this.xVel += 1.5
        }
        
    }

    setYVelocity(x)
    {
        this.yVel = x
    }

    setXVelocity(x)
    {
        this.xVel = x
    }

    setColor(x)
    {
        this.color = x
    }

    draw() 
    {
        console.log('playerexists')
        ctx.fillStyle = this.color;
        ctx.beginPath();
        ctx.lineTo(this.x,this.y)
        ctx.lineTo(this.x-30, this.y+30)
        ctx.lineTo(this.x+30, this.y+30)
        ctx.closePath()
        ctx.fill()
    }

    updateY()
    {
        //this.yVel += gravity/15
        this.y += this.yVel
        if(this.y+30>canvas.height-1)
        {
            this.y = canvas.height-31
            this.yVel *= -1
        }
        if(this.y<1)
        {
            this.y = 1
            this.yVel *= -1
        }
    }

    updateX()
    {
        this.x += this.xVel
        if(this.x+30>canvas.width)
        {
            this.x = canvas.width-31
            this.xVel *= -1
        }
        if(this.x-30<1)
        {
            this.x = 31
            this.xVel *= -1            
        }
    }
}

var player = new Player('green');

var player2 = new Player('purple', 700, 100);

window.addEventListener('keydown', function(event)
{
    keyPress.push(event.key)
    
    if (event.key == 'b') 
    {
        mouseBlock = new Block(lmx1, lmy1)
    }

    if(event.key == 'w')
    {
        player.moveUp()
    }

    if(event.key == 'a')
    {
        player.moveLeft()
    }

    if(event.key == 'd')
    {
        player.moveRight()
    }

    if(event.key == 's')
    {
        player.moveDown()
    }

})

window.addEventListener('keydown', function(event)
{
    keyPress.push(event.key)
    
    if (event.key == 'b') 
    {
        mouseBlock = new Block(lmx1, lmy1)
    }

    if(event.key == 'ArrowUp')
    {
        player2.moveUp()
    }

    if(event.key == 'ArrowDown')
    {
        player2.moveDown()
    }

    if(event.key == 'ArrowLeft')
    {
        player2.moveLeft()
    }

    if(event.key == 'ArrowRight')
    {
        player2.moveRight()
    }
})

window.addEventListener('keyup', function(event)
{
    
    for(let i = 0; i<keyPress.length; i++)
    {
        if(keyPress[i] == event.key)
        {
            keyPress.splice(i, 1)
        }
    }

    // if(event.key == 'w')
    // {
    //     player.setYVelocity(0)
    // }

    // if(event.key == 's')
    // {
    //     player.setYVelocity(0)
    // }



    if (event.key == 'b') 
    {
        let block = new Block(lmx1, lmy1)
        mouseBlock = null
        blocks[blockCounter] = block
        //alert(blockCounter)
        blockCounter++
    }

})

canvas.addEventListener('mousedown', function(event) 
{
    //console.log("Reached");
    //alert("dumb");
    var mouseX = event.clientX
    var mouseY = event.clientY
    mouseBall = new Ball(mouseX,mouseY);
})

canvas.addEventListener('mousemove', function(event) 
{
    var mouseX = event.clientX
    var mouseY = event.clientY

    if (mouseBall != null) 
    {
        mouseBall = new Ball(mouseX,mouseY)
    }
    if(mouseBlock != null)
    {
        mouseBlock = new Block(mouseX, mouseY)
    }
    lmx1 = mouseX
    lmy1 = mouseY
})

window.setInterval(function() {
    lmx3 = lmx2
    lmy3 = lmy2
    lmx2 = lmx1
    lmy2 = lmy1
    speedX = (lmx2 - lmx3)/2
    speedY = (lmy2 - lmy3)/2
}, interval)

canvas.addEventListener('mouseup', function(event) 
{

    var mouseX = event.clientX
    var mouseY = event.clientY

    speedX = (lmx2 - lmx3)/3
    speedY = (lmy2 - lmy3)/3

    let ball = new Ball(mouseX, mouseY)
    mouseBall = null
    balls[ballCounter] = ball
    ballCounter++

    // console.log(speedX + " " + speedY)
});

function main()
{
    requestAnimationFrame(main)

    ctx.clearRect(0,0,canvas.width,canvas.height)

    if (mouseBall != null) 
    {
        mouseBall.draw()
    }
    for (let i = 0; i<ballCounter; i++)
    {
        balls[i].update()
    }
    for(let i = 0; i<ballCounter; i++)
    {    
        balls[i].draw()
    }

    if(mouseBlock != null)
    {
        mouseBlock.draw()
    }

    for (let i = 0; i<blockCounter; i++)
    {
        blocks[i].update()
    }

    for (let i = 0; i<blockCounter; i++)
    {
        blocks[i].draw()
    }

    for(let i = 0; i < blocks.length-1; i++) 
    {
        for(let j = i+1; j < blocks.length; j++) 
        {
            if(blocks[i].collideBlock(blocks[j]))
                console.log("Block collision")
        }
    }

    player.updateY()
    player.updateX()

    player2.updateY()
    player2.updateX()

    player2.draw()
    player.draw()
    //console.log(balls[0].getY());
}

main()
