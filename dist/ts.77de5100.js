// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"index.ts":[function(require,module,exports) {
var canvas = document.querySelector('canvas');
var ctx = canvas.getContext('2d');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
var gravity = 3;
var balls = [];
var mouseBall = null;
var ballCounter = 0;
var mouseBlock = null;
var blocks = [];
var blockCounter = 0;
var interval = 50;
var lmx1, lmx2, lmx3;
var lmy1, lmy2, lmy3;
var timestamp;
var speedX;
var speedY;
var keyPress = [];

function keyPressed() {
  while (keyPress.length >= 1) {
    return true;
  }

  return false;
}

var Ball =
/** @class */
function () {
  function Ball(x, y, yVel, xVel) {
    if (x === void 0) {
      x = 0;
    }

    if (y === void 0) {
      y = 0;
    }

    if (yVel === void 0) {
      yVel = speedY;
    }

    if (xVel === void 0) {
      xVel = speedX;
    }

    this.x = x;
    this.y = y;
    this.yVel = yVel;
    this.xVel = xVel;
  }

  Ball.prototype.update = function () {
    if (this.y < canvas.height) {
      this.y += this.yVel;
      this.x += this.xVel;
      this.yVel += gravity;
    }

    if (this.y > canvas.height - 19) {
      this.y = canvas.height - 20;

      if (this.yVel > 50) {
        this.yVel = -this.yVel * 0.85;
      } else if (this.yVel > 30) {
        this.yVel = -this.yVel * 0.7;
      } else if (this.yVel > 15) {
        this.yVel = -this.yVel * 0.5;
      } else if (this.yVel > 7) {
        this.yVel = -this.yVel * 0.2;
      } else {
        this.yVel = -this.yVel * 0;
      }
    }

    if (this.y < 19) {
      this.y = 20;
      this.yVel *= -0.85;
    }

    if (this.x > canvas.width || this.x < 19) {
      if (this.x > canvas.width) {
        this.x = canvas.width - 1;
      } else {
        this.x = 20;
      }

      this.xVel *= -0.8;
    }
  };

  Ball.prototype.getY = function () {
    return this.y;
  };

  Ball.prototype.draw = function () {
    ctx.fillStyle = 'red';
    ctx.beginPath();
    ctx.arc(this.x, this.y, 20, 0, Math.PI * 2);
    ctx.closePath();
    ctx.fill();
  };

  return Ball;
}();

var Block =
/** @class */
function () {
  function Block(x, y, yVel) {
    if (x === void 0) {
      x = 0;
    }

    if (y === void 0) {
      y = 0;
    }

    if (yVel === void 0) {
      yVel = 0;
    }

    this.x = x;
    this.y = y;
    this.yVel = yVel;
  }

  Block.prototype.draw = function () {
    ctx.fillStyle = 'blue';
    ctx.beginPath();
    ctx.rect(this.x, this.y, 50, 50);
    ctx.closePath();
    ctx.fill();
  };

  Block.prototype.update = function () {
    // console.log(this.yVel);
    if (this.y < canvas.height) {
      // console.log('reached')
      this.y += this.yVel;
      this.yVel += gravity;
    }

    if (this.y > canvas.height - 49) {
      this.y = canvas.height - 50;

      if (this.yVel > 50) {
        this.yVel = -this.yVel * 0.85;
      } else if (this.yVel > 30) {
        this.yVel = -this.yVel * 0.7;
      } else if (this.yVel > 15) {
        this.yVel = -this.yVel * 0.5;
      } else if (this.yVel > 7) {
        this.yVel = -this.yVel * 0.2;
      }
    }
  };

  Block.prototype.collideBlock = function (block) {
    // [(x1, y1), (x2, y2)] top left and bottom right
    var c1 = [(this.x, this.y), (this.x + 50, this.y + 50)];
    var c2 = [(block.x, block.y), (block.x + 50, block.y + 50)];
    return c1[0][1] > c2[1][1] && c1[1][1] < c2[0][1] && c1[0][0] < c2[1][0] && c1[1][0] > c2[0][0];
  };

  return Block;
}();

var Player =
/** @class */
function () {
  function Player(x, y, xVel, yVel) {
    if (x === void 0) {
      x = 100;
    }

    if (y === void 0) {
      y = 100;
    }

    if (xVel === void 0) {
      xVel = 0;
    }

    if (yVel === void 0) {
      yVel = 0;
    }

    this.x = x;
    this.y = y;
    this.xVel = xVel;
    this.yVel = yVel;
  }

  Player.prototype.moveUp = function () {
    if (this.yVel >= -10) {
      this.yVel -= 3;
    }
  };

  Player.prototype.moveDown = function () {
    if (this.yVel <= 10) {
      this.yVel += 3;
    }
  };

  Player.prototype.moveLeft = function () {
    if (this.xVel >= -10) {
      this.xVel -= 1;
    }
  };

  Player.prototype.moveRight = function () {
    if (this.xVel <= 10) {
      this.xVel += 1;
    }
  };

  Player.prototype.setYVelocity = function (x) {
    this.yVel = x;
  };

  Player.prototype.setXVelocity = function (x) {
    this.xVel = x;
  };

  Player.prototype.draw = function () {
    console.log('playerexists');
    ctx.fillStyle = 'green';
    ctx.beginPath();
    ctx.lineTo(this.x, this.y);
    ctx.lineTo(this.x - 30, this.y + 30);
    ctx.lineTo(this.x + 30, this.y + 30);
    ctx.closePath();
    ctx.fill();
  };

  Player.prototype.updateY = function () {
    this.yVel += gravity / 10;
    this.y += this.yVel;

    if (this.y + 30 > canvas.height - 1) {
      this.y = canvas.height - 31;
      this.yVel *= -1;
    }

    if (this.y < 1) {
      this.y = 1;
      this.yVel *= -1;
    }
  };

  Player.prototype.updateX = function () {
    this.x += this.xVel;

    if (this.x + 30 > canvas.width) {
      this.x = canvas.width - 31;
      this.xVel *= -1;
    }

    if (this.x - 30 < 1) {
      this.x = 31;
      this.xVel *= -1;
    }
  };

  return Player;
}();

var player = new Player();
window.addEventListener('keydown', function (event) {
  keyPress.push(event.key);

  if (event.key == 'b') {
    mouseBlock = new Block(lmx1, lmy1);
  }

  if (event.key == 'w') {
    player.moveUp();
  }

  if (event.key == 'a') {
    player.moveLeft();
  }

  if (event.key == 'd') {
    player.moveRight();
  }

  if (event.key == 's') {
    player.moveDown();
  }
});
window.addEventListener('keyup', function (event) {
  for (var i = 0; i < keyPress.length; i++) {
    if (keyPress[i] == event.key) {
      keyPress.splice(i, 1);
    }
  } // if(event.key == 'w')
  // {
  //     player.setYVelocity(0)
  // }
  // if(event.key == 's')
  // {
  //     player.setYVelocity(0)
  // }


  if (event.key == 'b') {
    var block = new Block(lmx1, lmy1);
    mouseBlock = null;
    blocks[blockCounter] = block; //alert(blockCounter)

    blockCounter++;
  }
});
canvas.addEventListener('mousedown', function (event) {
  //console.log("Reached");
  //alert("dumb");
  var mouseX = event.clientX;
  var mouseY = event.clientY;
  mouseBall = new Ball(mouseX, mouseY);
});
canvas.addEventListener('mousemove', function (event) {
  var mouseX = event.clientX;
  var mouseY = event.clientY;

  if (mouseBall != null) {
    mouseBall = new Ball(mouseX, mouseY);
  }

  if (mouseBlock != null) {
    mouseBlock = new Block(mouseX, mouseY);
  }

  lmx1 = mouseX;
  lmy1 = mouseY;
});
window.setInterval(function () {
  lmx3 = lmx2;
  lmy3 = lmy2;
  lmx2 = lmx1;
  lmy2 = lmy1;
  speedX = (lmx2 - lmx3) / 2;
  speedY = (lmy2 - lmy3) / 2;
}, interval);
canvas.addEventListener('mouseup', function (event) {
  var mouseX = event.clientX;
  var mouseY = event.clientY;
  speedX = (lmx2 - lmx3) / 3;
  speedY = (lmy2 - lmy3) / 3;
  var ball = new Ball(mouseX, mouseY);
  mouseBall = null;
  balls[ballCounter] = ball;
  ballCounter++; // console.log(speedX + " " + speedY)
});

function main() {
  requestAnimationFrame(main);
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  if (mouseBall != null) {
    mouseBall.draw();
  }

  for (var i = 0; i < ballCounter; i++) {
    balls[i].update();
  }

  for (var i = 0; i < ballCounter; i++) {
    balls[i].draw();
  }

  if (mouseBlock != null) {
    mouseBlock.draw();
  }

  for (var i = 0; i < blockCounter; i++) {
    blocks[i].update();
  }

  for (var i = 0; i < blockCounter; i++) {
    blocks[i].draw();
  }

  for (var i = 0; i < blocks.length - 1; i++) {
    for (var j = i + 1; j < blocks.length; j++) {
      if (blocks[i].collideBlock(blocks[j])) console.log("Block collision");
    }
  }

  player.updateY();
  player.updateX();
  player.draw(); //console.log(balls[0].getY());
}

main();
},{}],"node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "49250" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else {
        window.location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["node_modules/parcel-bundler/src/builtins/hmr-runtime.js","index.ts"], null)
//# sourceMappingURL=/ts.77de5100.js.map